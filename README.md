# README #

### What is RabbitMQ? ###

RabbitMQ is open source message broker software (sometimes called message-oriented middleware) that implements the Advanced Message Queuing Protocol (AMQP). The RabbitMQ server is written in the Erlang programming language and is built on the Open Telecom Platform framework for clustering and failover. Client libraries to interface with the broker are available for all major programming languages.

### About this image ###

This image is based in Alpine Linux that's a Linux distribution built around musl libc and BusyBox. The Alpine's image is only 5 MB in size and has access to a package repository that is much more complete than other BusyBox based images.
After installation, RabbitMQ's image will have just 35.01mb.

### How to use this image ###

* git clone https://bitbucket.org/rafaelferrari/docker-rabbitmq
* cd docker-rabbitmq
* docker build -t="rabbitmq" .
* sudo docker run -d --hostname localhost --name rabbitmq -p 15672:15672 -p 5672:5672 rabbitmq