#!/bin/bash

# When this exits, exit all back ground process also.
trap 'kill $(jobs -p)' EXIT

# Die on error
set -e

# If long & short hostnames are not the same, use long hostnames
if ! [[ "$(hostname)" == "$(hostname -s)" ]]; then
    export RABBITMQ_USE_LONGNAME=true
fi

echo "Launching RabbitMQ..."
mv ${RABBITMQ_HOME}/etc/rabbitmq/standard.config \
    ${RABBITMQ_HOME}/etc/rabbitmq/rabbitmq.config

# RMQ server process so we can tail the logs
# to the same stdout
rabbitmq-server &

# Capture the PID
rmq_pid=$! &

# If RMQ dies, this script dies
wait $rmq_pid 2> /dev/null
